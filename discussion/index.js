console.log("helloworld");

//DIFFERENCE
/*
	1. quotation marks
		JS Objects only has the value inserted inside quotation marks
		JSON has the quotation marks for both key & the value
	
	2. 
	-JS Objects - exclusive to javascript, other programming languages cannot use js object files
	-JSON - not exclusive to javascript, other programming languages can also use JSON files
*/


/*
	-create & log a JS obj w/ the ff properties
	city
	province
	country
*/




/* let city = {
	"city": "Ormoc",
	"province": "Leyte",
	"country": "Philippines"
}
console.log(city); */


// JSON Object
/*
	JavaScript Object Notation
	JSON - used for serializing/deserializing different data types into byte
		 - byte - binary language ( 1 & 0) w/c is used to represent  .......
		Serialization - process of converting data into series of bytes for easier transmission or transfer of 
		information
*/

/*let city = {
	"city": "Ormoc",
	"province": "Leyte",
	"country": "Philippines"
};
console.log(city);*/


// JSON Arrays
/*let cities = [
{
	"city": "Ormoc",
	"province": "Leyte",
	"country": "Philippines"
},
{
	"city": "Bacoor",
	"province": "Cavite",
	"country": "Philippines"
},
{
	"city": "New York",
	"province": "New York",
	"country": "U.S.A."
}
];
console.log(cities);*/


// JSON METHODS

	// JSON obj contains methods for parsing & converting data into stringified JSON
	// Stringified JSON - JSON Object converted into string to be used in other functions of the 
		//language esp. javascript-based applications(serialize)

let batches = [
{
	"batchName": "Batch X"
},
{
	"batchName": "Batch Y"
}
];
console.log("Result from console.log method");
console.log(batches);

console.log("Result from stringify method");
// stringify - used to convert JSON Objects into JSON (string)
console.log(JSON.stringify(batches));

// using stringify for JS Objects
let data = JSON.stringify({
	name: "John",
	age: 31,
	address:{
		city: "Manila",
		country: "Philippines"
	}
});
console.log(data);



/*
Assignment
	create userDatails variable that will contain js object with the following properties
		fname - prompt
		lname - prompt
		age - prompt
		address:{
			city - prompt
			country - prompt
			zipCode - prompt
		}

	log in the console the converted JSON data type
*/
// =============================================================

// uncomment later!!
// let fname = prompt("Please input your first name.");
// let lname = prompt("Please input your last name.");
// let age = prompt("Please input your age.");
// let city = prompt("City");
// let country = prompt("Country");
// let zipCode = (prompt("Zipcode"));

// let userDatails = JSON.stringify({
// 	fname: fname,
// 	lname: lname,
// 	age: age,
// 	address:{
// 			city: city, 
// 			country: country,
// 			zipCode: zipCode
// 		}

// });
// console.log(userDatails);


// =============================================================

	

//Alternate Solution:
// let userDatails = JSON.stringify({
// 	fname:prompt("Please input your first name."),
// 	lname:prompt("Please input your last name."),
// 	age:prompt("Please input your age."),  //parseInt already useless; only used when numerical 
// 										   //data is needed
// 	address:{
// 			city:prompt("City"),
// 			country:prompt("Country"),
// 			zipCode:prompt("Zipcode")
// 		}

// });
// console.log(userDatails);	


// =============================================================

// CONVERTING OF STRINGIFIED JSON INTO JS OBJECTS
	// PARSE Method - converting json data into js objects
	// info is commonly sent to application in STRINGIFIED JSON; then converted into objects;
	//	this happens both for sending info to a backend app such as databases & back to 
	//	frontend app such as the webpages
	//	upon receiving the data, JSON txt can be converted into a JS/JSON Object w/ parse 
	//	method

let batchesJSON = `[
{
	"batchName": "Batch X"
},
{
	"batchName": "Batch Y"
}
]`;
console.log(batchesJSON);

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));    //convert fr stringified JSON to JS obj
// ********* ++output++ ************
// (2) [{…}, {…}]
// 0: {batchName: 'Batch X'}
// 1: {batchName: 'Batch Y'}
// length: 2
// [[Prototype]]: Array(0)
// *********************************

let stringifiedData = `{
	"name": "John",
	"age": "31",
	"address":{
		"city": "Manila",
		"country": "Philippines"
	}
}`;
//how do we convert the JSON string above into a JS obj & log it in our console?
console.log(JSON.parse(stringifiedData));







